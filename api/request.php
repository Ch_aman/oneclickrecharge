<?php  
require_once 'config_api.php';
require_once LIB_PATH.'fetchResponce.php'; 
$data_request = inputs();

if(!$data_request){
	echo json_encode(array('status' => 'F', 'msg' => 'Request data is not properly formatted', 'error' => 'Request data is not properly formatted', 'errorCode' => '1001'));
	exit();
}
GLOBAL $DB;
require_once LIB_PATH.'functions_service.php';  
require_once LIB_PATH.'class.oneCrApp.php'; 


$request 			 = saveApiLog($data_request); 

$apikey 	= (isset($request['apikey'])) ? $request['apikey'] : '';
$method 	= (isset($request['method'])) ? $request['method'] : '';

if ($apikey == '' || $method == ''){
	$result = array('status' => 'F', 'msg' => 'apikey and method are compulsory parameters.', 'error' => 'apikey, method compulsory parameters.', 'errorCode' => '002');
	output($result);
}

$objApp  = new onecrApp();
$res_apikey	= $objApp->validateApiKey($apikey);
if ($res_apikey['status'] == 'F') {  // if status is false while checking API KEY show error
	output($res_apikey);
}

if ($res_apikey['status'] == 'F') {  // if status is false while checking Model show error

} else {
	switch (trim($method)) {
		///Save inspection custoemr details
		case "addUser":
			$result =  $objApp->addUser($request);
			header('Content-Type: application/json');
			output($result);
			break;	
		case "saveLead":
			$result =  $objApp->saveLead($request);
			header('Content-Type: application/json');
			output($result);
			break;
		case "doTranscation":
			$result =  $objApp->doTranscation($request);
			header('Content-Type: application/json');
			output($result);
			break;	
		case "checkLogin":
			$result =  $objApp->checkLogin($request);
			header('Content-Type: application/json');
			output($result);
			break;
		case "verifyOTP":
			$result =  $objApp->verifyOTP($request);
			header('Content-Type: application/json');
			output($result);
			break;	
		case "getHotelCity":
			$result =  $objApp->getHotelCity($request);
			output($result);
			break;
		case "getHotelCity":
			$result =  $objApp->getHotelCity($request);
			output($result);
			break;		
	
		default:
			$result = array('status' => 'F', 'msg' => 'You Have Entered Invalid method!!', 'error' => 'You Have Entered Invalid method!!', 'errorCode' => '1002');
			header('Content-Type: application/json');
			output($result);
			break;
	}
}
?>