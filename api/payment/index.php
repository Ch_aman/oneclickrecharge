<?php
  /*
   *  @author   Aman Nehra
   *  @about    PayUbizz Payment Gateway integration in PHP
   */
  require_once '../config_api.php'; GLOBAL $DB; 
	$lead_id   = $data['data']['lead_id'];
	$amount    = $data['data']['amount'];
	
	if ($lead_id == '' || $lead_id == null) {
			return $result = array('status' => 'F', 'msg' => 'Please Enter LEAD ID',
				'error' => 'LEAD ID is required field', 'errorCode' => '1022');
	}
	if ($amount == '' || $amount == null) {
			return $result = array('status' => 'F', 'msg' => 'Please Enter Amount',
				'error' => 'Amount is required field', 'errorCode' => '1023');
	}

	
	$merchant_key  = MERCHANT_KEY;
	$salt          = MERCHANT_SALT;
	$payu_base_url = "https://secure.payu.in"; 
	$action        = '';
	$currentDir	   = PAYMENT_DIR;
	$posted = array();
	if(!empty($_POST)) {
	  foreach($_POST as $key => $value) {    
	    $posted[$key] = $value; 
	  }
	}

	$formError = 0;
	if(empty($posted['txnid'])) {
	  $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
	} else {
	  $txnid = $posted['txnid'];
	}

	$hash         = '';
	$hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";

	if(empty($posted['hash']) && sizeof($posted) > 0) {
	  if(
          empty($posted['key'])
          || empty($posted['txnid'])
          || empty($posted['amount'])
          || empty($posted['firstname'])
          || empty($posted['email'])
          || empty($posted['phone'])
          || empty($posted['productinfo'])
          || empty($posted['surl'])
          || empty($posted['furl'])
	  ){
	    $formError = 1;

	  } else {
	   	$hashVarsSeq = explode('|', $hashSequence);
	    $hash_string = '';	
		foreach($hashVarsSeq as $hash_var) {
	      $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
	      $hash_string .= '|';
	    }
	    $hash_string .= $salt;
	    $hash = strtolower(hash('sha512', $hash_string));
	    $action = $payu_base_url . '/_payment';
	  }
	} elseif(!empty($posted['hash'])) {
	  $hash = $posted['hash'];
	  $action = $payu_base_url . '/_payment';
	}
	
//$posted['amount'] 		= '1';
//$posted['firstname'] 	= 'check';
//$posted['email'] 		= 'test@test.com';
//$posted['phone'] 		= '7289009620';
//$posted['productinfo']  =  '1CR';

$select_user_dtl = "select lead_mobile,user_name,user_email from cr_lead join cr_user on (cr_user.user_id=cr_lead.user_id) where lead_id = '".$lead_id."'"; 
$res_user_dtl = $DB->Query($select_user_dtl);

$posted = array();
$posted['amount'] 		= $data['data']['amount'];
$posted['firstname'] 	= $res_user_dtl[0]->user_name;
$posted['email'] 		= $res_user_dtl[0]->user_email;
$posted['phone'] 		= $res_user_dtl[0]->lead_mobile;
$posted['productinfo']  =  '1CR';

			$lead_fields = array(0 =>'lead_id', 1 => 'trans_payment', 2=>'trans_params', 3=>'trans_date');
			$lead_values = array(0 =>$lead_id, 1 => $amount, 2=>$customer_id, 3=> date('Y-m-d H:i:s'));
			$txnid = $DB->insert('cr_transaction',$lead_fields,$lead_values);
		
?>
<html>
  <head>
  <script>
    //var hash = '<?php echo $hash ?>';
    //function submitPayuForm() {
    //  if(hash == '') {
    //    return;
    //  }
    //  var payuForm = document.forms.payuForm;
    //  payuForm.submit();
    //}
		//document.payuForm1cr.submit();
  </script>
  </head>
  <body>
    <h2><center>Your are redirecting to payment gateway...</center></h2>
    <br/>
    <?php if($formError) { ?>
      <span style="color:red">Please fill all mandatory fields.</span>
      <br/>
      <br/>
    <?php } ?>
    <form action="<?php echo $action; ?>" method="post" name="payuForm1cr">
      <input type="hidden" name="key" value="<?php echo $merchant_key ?>" />
      <input type="hidden" name="hash" value="<?php echo $hash ?>"/>
      <input type="hidden" name="txnid" value="<?php echo $txnid ?>" />
      <table style="display:none;">
		<tr  style="display: none">
          <td><b>Mandatory Parameters</b></td>
        </tr>
        <tr>
          <td>Amount <span class="mand">*</span>: </td>
          <td><input name="amount" type="text" value="<?php echo (empty($posted['amount'])) ? '' : $posted['amount'] ?>" /></td>
		  </tr><br />
		  <tr>
          <td>Name <span class="mand">*</span>: </td>
          <td><input type="text" name="firstname" id="firstname" value="<?php echo (empty($posted['firstname'])) ? '' : $posted['firstname']; ?>" /></td>
        </tr><br />
        <tr> 
          <td>Email <span class="mand">*</span>: </td>
          <td><input type="email" name="email" id="email" value="<?php echo (empty($posted['email'])) ? '' : $posted['email']; ?>" /></td>
		  </tr><tr>
          <td>Phone <span class="mand">*</span>: </td>
          <td><input type="text" name="phone" value="<?php echo (empty($posted['phone'])) ? '' : $posted['phone']; ?>" /></td>
        </tr>
        <tr>
          <td>Product Info <span class="mand">*</span>: </td>
          <td colspan="3"><textarea name="productinfo"><?php echo (empty($posted['productinfo'])) ? '' : $posted['productinfo'] ?></textarea></td>
        </tr>
        <tr style="display: none">
          <td>Success URL <span class="mand">*</span>: </td>
          <td colspan="3"><input type="text" name="surl" value="<?php echo (empty($posted['surl'])) ? $currentDir.'success.php' : $posted['surl'] ?>" size="64" /></td>
        </tr>
        <tr style="display: none">
          <td>Failure URL <span class="mand">*</span>: </td>
          <td colspan="3"><input type="text" name="furl" value="<?php echo (empty($posted['furl'])) ? $currentDir.'failure.php' : $posted['furl'] ?>" size="64" /></td>
        </tr>

        <tr>
          <td colspan="3"><input type="hidden" name="service_provider" value="" size="64" /></td>
        </tr>

        <tr style="display: none">
          <td><b>Optional Parameters</b></td>
        </tr>
        <tr style="display: none">
          <td>Last Name: </td>
          <td><input type="text" name="lastname" id="lastname" value="<?php echo (empty($posted['lastname'])) ? '' : $posted['lastname']; ?>" /></td>
          <td>Cancel URI: </td>
          <td><input type="text" name="curl" value="" /></td>
        </tr>
        <tr style="display: none">
          <td>Address1: </td>
          <td><input type="text" name="address1" value="<?php echo (empty($posted['address1'])) ? '' : $posted['address1']; ?>" /></td>
          <td>Address2: </td>
          <td><input type="text" name="address2" value="<?php echo (empty($posted['address2'])) ? '' : $posted['address2']; ?>" /></td>
        </tr>
        <tr style="display: none">
          <td>City: </td>
          <td><input type="text" name="city" value="<?php echo (empty($posted['city'])) ? '' : $posted['city']; ?>" /></td>
          <td>State: </td>
          <td><input type="text" name="state" value="<?php echo (empty($posted['state'])) ? '' : $posted['state']; ?>" /></td>
        </tr>
        <tr style="display: none">
          <td>Country: </td>
          <td><input type="text" name="country" value="<?php echo (empty($posted['country'])) ? '' : $posted['country']; ?>" /></td>
          <td>Zipcode: </td>
          <td><input type="text" name="zipcode" value="<?php echo (empty($posted['zipcode'])) ? '' : $posted['zipcode']; ?>" /></td>
        </tr>
        <tr style="display: none">
          <td>UDF1: </td>
          <td><input type="text" name="udf1" value="<?php echo (empty($posted['udf1'])) ? '' : $posted['udf1']; ?>" /></td>
          <td>UDF2: </td>
          <td><input type="text" name="udf2" value="<?php echo (empty($posted['udf2'])) ? '' : $posted['udf2']; ?>" /></td>
        </tr>
        <tr style="display: none">
          <td>UDF3: </td>
          <td><input type="text" name="udf3" value="<?php echo (empty($posted['udf3'])) ? '' : $posted['udf3']; ?>" /></td>
          <td>UDF4: </td>
          <td><input type="text" name="udf4" value="<?php echo (empty($posted['udf4'])) ? '' : $posted['udf4']; ?>" /></td>
        </tr>
        <tr style="display: none">
          <td>UDF5: </td>
          <td><input type="text" name="udf5" value="<?php echo (empty($posted['udf5'])) ? '' : $posted['udf5']; ?>" /></td>
          <td>PG: </td>
          <td><input type="text" name="pg" value="<?php echo (empty($posted['pg'])) ? '' : $posted['pg']; ?>" /></td>
        </tr>
        <!--<tr>
          <?php if(!$hash) { ?>
            <td colspan="4"><input type="submit" value="Submit" /></td>
          <?php } ?>
        </tr>-->
      </table>
    </form>
  </body>
</html>
  <script>
    //var hash = '<?php echo $hash ?>';
    //function submitPayuForm() {
    //  if(hash == '') {
    //    return;
    //  }
    //  var payuForm = document.forms.payuForm;
    //  payuForm.submit();
    //}
		document.payuForm1cr.submit();
  </script>
