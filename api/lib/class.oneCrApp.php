<?php
class onecrApp {
	function validateApiKey($apikey) { 
		$result = '';
		if ($apikey == '') {
			$result = array('status' =>  'F', 'msg' => 'Please provide API Key.', 'error'=>'', 'errorCode' => '003');
		} else {
			$arr_client_index = $this->getClientIdAndKey($apikey);
			$arr_client       = $arr_client_index[0];
			if($arr_client){
				if($arr_client->client_status==0){
					$result = array('status' => 'F', 'msg' => 'Inactive Client id', 'error'=>'Inactive Client id', 'errorCode' => '003');
				}else{
					$GLOBALS['client_id'] = $arr_client->client_id;
					$result = array('status' =>  'T', 'msg' => 'API Key Matched.', 'error'=>'', 'errorCode'=>'');
				}			
			}else{
				$result = array('status' => 'F', 'msg' => 'Invalid API Key.', 'error'=>'Invalid API Key.', 'errorCode' => '003');
			}
		}
		return $result;
	}
	
	public function getClientIdAndKey($apikey) {
		global $DB;
		$query = "select `client_id`, `client_status` from cr_client where client_key = '$apikey'";
		$result = $DB->query($query);
		return $result;
	}
	
	public function addUser($data) {
		global $DB;
		global $obj_ses;
		
		$result 			= array();
		$client_id 			= $GLOBALS['client_id'];
		$user_name 			= $data['data']['name'];
		$user_mobile 		= $data['data']['mobile'];
		$user_email 		= $data['data']['email'];
		$user_gcmid 		= $data['data']['gcmid'];
		$user_imei 			= $data['data']['imei'];
		$user_loin_source 	= $data['data']['login_source'];
		$user_city 			= $data['data']['city'];
		$source 			= $data['SOURCE'];

		##################UPDATE USER #########################
		/*$select_exits_user = "select `user_id`, `user_source` from cr_user where user_email = '$user_email'";
		$result_exits_user = $DB->fetchRowsAssoc($select_exits_user);

		if($queryresult[0]){
			if(strtoupper($user_source) == 'FACEBOOK' || strtoupper($user_source) == 'GOOGLE' || strtoupper($user_source) == 'WEB' ){
				if($user_gcmid !=''){
					$updsategcm = "update cr_user set user_gcmid = '$user_gcmid', `login_source` = '$user_loin_source' where user_id =".$queryresult[0]['user_id'];
					$resultupdategcm = $db->Query($updsategcm);
				}
				return $result = array('status' => 'T', 'msg' => 'Registration Successful',
				'error' => '', 'user_id' => $queryresult[0]['user_id']);
			}else{
				$otpdata['data']['user_id'] = $queryresult[0]['user_id'];
				//$this->getOTP($otpdata);
				//return $result = array('status' => 'F', 'msg' => 'Email Id already exist','error' => 'Please enter another email Id or Go to Forget Pasword', 'errorCode' => '015', 'user_id' => $queryresult[0]['user_id'], 'user_type' => $queryresult[0]['user_type']);
				return $result = array('status' => 'T', 'msg' => 'Registration Successful','error' => '', 'errorCode' => '', 'user_id' => $queryresult[0]['user_id']);
				
			}
		}*/
		
		if ($user_name == '' || $user_name == null) {
			return $result = array('status' => 'F', 'msg' => 'Please Enter Name',
				'error' => 'Name is required field', 'errorCode' => '1010');
		}

		if (!ctype_alpha(str_replace(' ', '', $user_name))) {
			return $result = array('status' => 'F', 'msg' => 'Name Should contains alphabets only',
				'error' => 'Please Enter Name In alphabets', 'errorCode' => '1011');
		}

		$regex = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/';
		if (!preg_match($regex, $user_email)) {
			return $result = array('status' => 'F', 'msg' => 'Email is not valid',
      			 'error' => 'Please Enter valid Email', 'errorCode' => '1012');
		}
		
		if ($user_mobile == '' || $user_mobile == null) {
			return $result = array('status' => 'F', 'msg' => 'Mobile should not be empty',
				'error' => 'Please Enter valid  mobile', 'errorCode' => '1013');
		}
		
		if (!is_numeric($user_mobile)) {
			return $result = array('status' => 'F', 'msg' => 'Mobile should be numeric value',
				'error' => 'Please Enter valid  mobile', 'errorCode' => '1014');
		}
		
		if (strlen($user_mobile) != 10) {
			return $result = array('status' => 'F', 'msg' => 'Mobile should be 10 digit number',
				'error' => 'Please Enter valid  mobile of 10 digits', 'errorCode' => '1015');
		}
	
		if ($user_gcmid == ''|| $user_gcmid == null) {
			return $result = array('status' => 'F', 'msg' => 'Please Enter GCMID',
				'error' => 'gcmid is required variable', 'errorCode' => '1016');
		}
		
		if ($user_imei == '' || $user_imei == null) {
			return $result = array('status' => 'F', 'msg' => 'Please Enter IMEI number',
				'error' => 'IMEI number is required field', 'errorCode' => '1017');
		}

		if ($user_loin_source == '' || $user_loin_source == null) {
			return $result = array('status' => 'F', 'msg' => 'Source is required variable',
				'error' => 'Please Enter Source', 'errorCode' => '1018');
		}
		
		if (strtoupper($user_loin_source) == 'FACEBOOK' || strtoupper($user_loin_source) == 'GOOGLE' || strtoupper($user_loin_source) == 'DIRECT' || strtoupper($user_loin_source) == 'WEB'){
            //do nothing
        }else{
            return $result = array('status' => 'F', 'msg' => 'Invalid Source',
                'error' => 'Please Enter Valid Source', 'errorCode' => '1019');
        }
		$fields = array(0 =>'user_name', 1 => 'user_email', 2=>'user_mobile', 3=>'user_gcmid', 4=>'user_login_source', 5=>'user_source', 6=>'user_city', 7=>'user_type', 8=>'otp_verified', 9=>'added_date', 10=>'user_imei');
		$values = array(0 =>$user_name, 1 => $user_email, 2=>$user_mobile, 3=>$user_gcmid, 4=>$user_loin_source, 5=>$source, 6=>$user_city, 7=> 'consumer', 8=>'0',9=> date('Y-m-d H:i:s'), 10=>$user_imei);
		$user_id = $DB->insert('cr_user',$fields,$values);
		
		######################MAILER CODE##########################
		/*if(!empty($user_id)){
			
			if (strtoupper($user_loin_source) == 'FACEBOOK' || strtoupper($user_loin_source) == 'GOOGLE'){
				$mobile = $user_mobile;
				$message = urlencode('Thank you for signing up on the '.MODEL_NAME.' App. Please check your mail for further details.');
				$statsmsg = $this->sendCommonSMS( $mobile, $message) ;
				
				$to = $field_email;
				$subject = 'Welcome to '.MODEL_NAME;
				include_once('add_user_emailer.php');
				$template_name = EMAIL_TEMPLETE;
				$responceEmail = $this->sendEmailThroughNotification($to, $subject, $user_mail, $sender = EMAILSENDER, '',$template_name);
			}
			
			$otpdata['data']['user_id'] = $user_id;
			$otpdata['model_id'] = $data['model_id'];
			if (strtoupper($user_loin_source) == 'DIRECTSOURCE'){
				$this->getOTP($otpdata);
			}
												
		}*/
		if(strtoupper($user_loin_source) == 'DIRECT') {
			$this->sendOTP($user_mobile, $user_email, $user_name,$user_id,$otp_type='login');
		}
		
		if($user_id){
		$result['status'] = 'T';
		$result['msg'] = 'Registration Successful';
		$result['user_id'] = $user_id;
		$result['user_type'] = 'Consumer';
		} else {
		$result['status'] = 'F';
		$result['msg'] = 'Failed.! Registration';
		$result['user_id'] = '';
		$result['user_type'] = 'Consumer';
			
		}
		return $result;
	}
	
	public function saveLead($data){
		global $DB;
		$user_id 		= $data['data']['user_id'];
		$lead_mobile 	= $data['data']['lead_mobile'] ? $data['data']['lead_mobile'] : '';
		$customer_id 	= $data['data']['customer_id'] ? $data['data']['customer_id'] : '';
		$lead_type	 	= $data['data']['lead_type'];
		$lead_source	= $data['data']['lead_source'] ? $data['data']['lead_source'] : '';
		$lead_operator	= $data['data']['lead_operator'] ? $data['data']['lead_operator'] : '';
		$lead_city	 	= $data['data']['lead_city'] ? $data['data']['lead_city'] : '';
		$lead_amount	= $data['data']['lead_amount'];
		if ($user_id == '' || $user_id == null) {
			return $result = array('status' => 'F', 'msg' => 'Please Enter user ID',
				'error' => 'User ID is required field', 'errorCode' => '1019');
		}
		
		if ($lead_type == '' || $lead_type == null) {
			return $result = array('status' => 'F', 'msg' => 'Please Enter lead type',
				'error' => 'Lead type is required field', 'errorCode' => '1020');
		}

		if ($lead_amount == '' || $lead_amount == null) {
			return $result = array('status' => 'F', 'msg' => 'Please Enter lead amount',
				'error' => 'Lead amount is required field', 'errorCode' => '1021');
		}
		
			$lead_fields = array(0 =>'user_id', 1 => 'lead_mobile', 2=>'customer_id', 3=>'lead_type', 4=>'lead_source', 5=>'lead_operator', 6=>'lead_city', 7=>'lead_amount', 8=>'create_date');
			$lead_values = array(0 =>$user_id, 1 => $lead_mobile, 2=>$customer_id, 3=>$lead_type, 4=>$lead_source, 5=>$lead_operator, 6=>$lead_city, 7=> $lead_amount, 8=> date('Y-m-d H:i:s'));
		
			$lead_id = $DB->insert('cr_lead',$lead_fields,$lead_values);
			
		if($lead_id){
			$result['status'] = true;
			$result['msg'] = 'Lead Created Successful';
			$result['lead_id'] = $lead_id;
			$result['lead_type'] = $lead_type;
		} else {
			$result['status'] = false;
			$result['msg'] = 'Failed.! Lead not created';
			$result['lead_id'] = '';
			$result['lead_type'] = $lead_type;
		}
		return $result;
	}
	
	public function doTranscation($data){
		global $DB;
		require_once('../payment/index.php');
	}
	
	public function checkLogin($data){
		global $DB;
		$result = array();
		$email  = $data['data']['email'];
		$mobile = $data['data']['mobile'];
	
		if (($email == '') && ($mobile == '')) {
			return $result = array('status' => 'F', 'msg' => 'Please Enter user email or user mobile',
				'error' => 'user email / user mobile is required field', 'errorCode' => '1019');
		}

		$select_user = "select user_id,user_source,user_login_source from cr_user where user_email = '".$email."' or user_mobile = '".$mobile."'";
		$res_check_user = $DB->Query($select_user);
				
		if(!empty($res_check_user[0])){
			$result['status'] 				= true;
			$result['msg'] 					= 'User Already Exist';
			$result['user_id'] 				= $res_check_user[0]->user_id;
			$result['login_source'] 		= $res_check_user[0]->user_login_source;
			$result['source'] 				= $res_check_user[0]->user_source;
			} else {
			$result['status'] 				= false;
			$result['msg'] 					= 'Failed.! User Not Exist';
			$result['user_id'] 				= $res_check_user[0]->user_id;
			$result['login_source'] 		= $res_check_user[0]->user_login_source;
			$result['user_source'] 			= $res_check_user[0]->user_source;
		}
		return $result;
		
		}
	public function getHotelCity($data) {
		global $DB;
		$value_1 = $data['data']['USERVAR1'];
		$value_2 = $data['data']['USERVAR2'];
		$value_3 = $data['data']['USERVAR3'];
		    $c_url = "https://rechargkit.biz/get/hotel/domestic/cityDetails?partner_id=".PARTNER_API_ID."&api_password=".PARTNER_API_PWD."&user_var1=$value_1&user_var2=$value_2&user_var3=$value_3";
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $c_url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
			$curl_results = curl_exec($ch);
			curl_close($ch);
			$result = json_decode($curl_results);
		return $result;
	}
	
		
		function sendOTP($mobile, $user_email, $user_name,$user_id,$otp_type){
			global $DB;
			global $obj_ses;
			$otp_code 	= $this->gererateOTP();
			$valid_till = date("d-M-Y H:i:s",strtotime('1 hour'));
			$otp_fields = array(0 =>'user_id', 1 => 'otp_code', 2=>'otp_mobile', 3=>'otp_email', 4=>'otp_type', 5=>'date_create');
			$otp_values = array(0 =>$user_id, 1 => $otp_code, 2=>$mobile, 3=>$user_email, 4=>$otp_type, 5=>$valid_till);
			$otp_id 	= $DB->insert('cr_otp',$otp_fields,$otp_values);
			$subject	= "OTP From 1CRAPP";           	
			$body		= "Dear ".ucwords($user_name).", <br /><br /> Your one time password is <b><u>$otp_code</u></b> and it is valid till $valid_till. Please do not share this OTP with anyone.<br><br>Thanks you 1CRAPP.";    	
			$mail_status	= $obj_ses->sendMail($sender="One Click Recharge <1CRAPP@www.1crapp.com>",$subject,$user_email,$cc='',$body);
		////// Otp ///////

		$message = ('Dear '.ucwords($user_name).', Your one time password is '.$otp_code.' and it is valid till '.$valid_till.'. Please do not share this OTP with anyone Thanks you 1CRAPP.');
		$statsmsg = $this->sendSMS ($message, $mobile) ;
		if($statsmsg){
			$is_mobile = true;
			$sqlCheck = "Update cr_otp SET otp_mobile_status = 1 where id =$otp_id";
			$queryresultinsemsg = $DB->query($sqlCheck);
		}
		if($mail_status){
		    $sqlCheck = "Update cr_otp SET otp_email_status = 1 where id =$otp_id";
			$queryresultinsemail = $DB->query($sqlCheck);
		}	
		return true;
	 }
	 
	  function verifyOTP($data) {
		global $DB;
		$user_id 	= $data['data']['user_id']; 
		$otp_mobile = $data['data']['mobile'];
		$otp_code 	= $data['data']['otp_code'];
		$till_now   = date("Y-m-d H:i:s");
		
		if ($user_id == '') {
			return $result = array('status' => 'F', 'msg' => 'Please Enter user ID',
				'error' => 'User ID is required field', 'errorCode' => '1050');
		}

		if ($otp_mobile == '') {
			return $result = array('status' => 'F', 'msg' => 'Please Enter Mobile Number',
				'error' => 'Mobile Number is required field', 'errorCode' => '1051');
		}

		if ($otp_code == '') {
			return $result = array('status' => 'F', 'msg' => 'Please Enter OTP Code',
				'error' => 'OTP Code is required field', 'errorCode' => '1052');
		}
		
		echo $check_otp = "select id from cr_otp where otp_code = '".$otp_code."' and user_id = '".$user_id."' and otp_mobile = '".$otp_mobile."' AND  `date_create` >= (DATE_sub(NOW(), INTERVAL 59 MINUTE))  order by id DESC limit 0,1";
		$res_otp = $DB->query($check_otp);
		if($res_otp){
			$result['status'] 				= true;
			$result['msg'] 					= 'Successfully verified';
			$result['id'] 					= $res_otp[0]->id;
		} else {
			$result['status'] 				= false;
			$result['msg'] 					= 'Soory, You have entered wrong OTP.';
			$result['id'] 					= '';
		}
		
		return $result;
	}
	 
	 function sendSMS($message,$mobile_number) {
		global $DB;
		$username	=	"oneclickrecharge";
		$password	=	"hranjan07291@";
		$sender		=	"ONECRP"; 
		
		$url="login.bulksmsgateway.in/sendmessage.php?user=".urlencode($username)."&password=".urlencode($password)."&mobile=".urlencode($mobile_number)."&message=".urlencode($message)."&sender=".urlencode($sender)."&type=".urlencode('3');
		
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$curl_scraped_page = curl_exec($ch);
		curl_close($ch);
		$fields = array(0 =>'response');
		$values = array(0 =>$curl_scraped_page);
		$msg_id 	= $DB->insert('cr_otp_log',$fields,$values);
		return $msg_id;
	}

		function gererateOTP(){
			$chars =  '123456789';
			$str = '';
			$max = strlen($chars) - 1;
			for ($i=0; $i < 6; $i++)
				$str .= $chars[mt_rand(1, $max)];
			return $str;
		}
	}
?>
