<?php
function get_request_method(){
	//return $_SERVER['REQUEST_METHOD'];
	return 'POST';
}

function inputs(){
	switch(get_request_method()){
		case "POST":
			$request = cleanInputs(json_decode($_POST['DaTa'], true));
			break;
		case "GET":
			$request = "Invalid Method 'use POST only':used GET";
			break;
		default:
			$request = "Invalid Method 'use POST only'";
			break;
	}

	return $request;
}
function cleanInputs($data){
	$clean_input = array();
	if(is_array($data)){
		foreach($data as $k => $v){
			$clean_input[$k] = cleanInputs($v);
		}
	}else{
		if(get_magic_quotes_gpc()){
			$data = trim(stripslashes($data));
		}
		$data = strip_tags($data);
		$clean_input = trim($data);
	}
	return $clean_input;
}	
?>